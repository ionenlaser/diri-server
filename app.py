import logging
import os

from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import DatabaseError, PendingRollbackError, DisconnectionError
from sqlalchemy.pool import NullPool

import local_config
import responses.pronunciations
import responses.spellings
import responses.user
import responses.wd_sparql_query as query


logging.basicConfig(level=logging.INFO)
db_string = "mysql+pymysql://%s:%s@%s/%s" % (local_config.user,
                                             local_config.password,
                                             local_config.host,
                                             local_config.database)
app = Flask(__name__)
logging.info("Set upload dir")
UPLOAD_DIR = str(os.path.abspath(os.getcwd())) + "/upload"
app.config.update({
    #    'SQLALCHEMY_POOL_TIMEOUT': None,
    'SQLALCHEMY_ENGINE_OPTIONS': {
        'poolclass': NullPool,
        #        'pool_size': None,
        #        'pool_recycle': None,
        #        'pool_pre_ping': False
    },
    'SQLALCHEMY_DATABASE_URI': db_string,
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'UPLOAD_FOLDER': UPLOAD_DIR + '/',
    'MAX_CONTENT_PATH': 1024 * 1024,
    'DEBUG': True,  # todo: remove
})


logging.info("Set up database connection")
db = SQLAlchemy(app)


class User(db.Model):
    uuid = db.Column(db.String(36), primary_key=True, nullable=False)
    user_lang = db.Column(db.String(2), nullable=False)
    game_lang = db.Column(db.String(2), nullable=False)
    points = db.Column(db.Integer, default=0)
    modified_at = db.Column(db.TIMESTAMP, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.uuid


class Pronunciation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # unique=True, deactivated for now
    lexeme = db.Column(db.String(120), nullable=False)
    recording = db.Column(db.String(120), unique=True)
    ipa = db.Column(db.String(80), nullable=False)
    language = db.Column(db.String(2), nullable=False,
                         server_default="en", default="en")

    def __repr__(self):
        return '<Pronunciations %r>' % self.id


class PronunciationInteraction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String(36), db.ForeignKey(User.uuid))
    pro_id = db.Column(db.Integer, db.ForeignKey(Pronunciation.id))
    recording = db.Column(db.String(120), unique=True)
    skipped = db.Column(db.Boolean)

    def __repr__(self):
        return '<PronunciationInteraction %r>' % self.id


class SpellingInteraction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    receiving_user = db.Column(db.String(36))
    pro_int_id = db.Column(
        db.Integer, db.ForeignKey(PronunciationInteraction.id))
    recording = db.Column(db.String(120))
    correction_rec = db.Column(db.String(120), unique=True)
    corrected = db.Column(db.Boolean)
    lexeme = db.Column(db.String(120))
    ipa = db.Column(db.String(80))
    inputs = db.Column(db.String(120))
    skipped = db.Column(db.Boolean)
    status = db.Column(db.String(120))

    def __repr__(self):
        return '<SpellingInteraction %r>' % self.id


logging.info("Create tables if needed")
db.create_all()

if Pronunciation.query.first() is None:
    logging.info("Initializing empty database")
    langs = ["de", "en"]
    for lang in langs:
        query_result = query.download_ipa_examples(lang)
        for line in query_result:
            try:
                if len(line["lemma"]["value"]) > 2 and len(line["ipa"]["value"]) > 2:
                    new_pro_obj = Pronunciation(ipa=line["ipa"]["value"],
                                                lexeme=line["lemma"]["value"],
                                                language=line["lemma"]["xml:lang"])
                    db.session.add(new_pro_obj)
                    db.session.commit()
            except Exception as e:
                logging.error(e)
                db.session.rollback()

test_uuid = "e15db9be-11d8-46ab-a52c-20d9252b7ed7"
if User.query.get(test_uuid) is None:
    logging.info("Add default test user account")
    default_user = User(uuid=test_uuid, game_lang="de", user_lang="en")
    db.session.add(default_user)
db.session.commit()


@app.route('/')
def hello_world():
    return 'This is the webserver for the diri project', 200


@app.route('/pronunciation', methods=["GET"])
def p_download():
    return responses.pronunciations.p_download()


@app.route('/pronunciation', methods=["POST"])
def p_upload():
    return responses.pronunciations.p_upload()


@app.route('/user', methods=["GET"])
def user():
    return responses.user.user_management()


@app.route('/spelling', methods=["GET"])
def s_download():
    return responses.spellings.s_download()


@app.route('/spelling', methods=["POST"])
def s_upload():
    return responses.spellings.s_upload()


@app.route('/resetr', methods=["GET"])
def reset():
    db.session.rollback()
    logging.info("Rolling back, because of command")
    return {"status": "ok"}, 200


@app.errorhandler(500)
def internal_error(error):
    logging.error(
        "Internal error, rolling back last transaction, error message was: " + str(error))
    db.session.rollback()
    return render_template('500.html'), 500


@app.errorhandler(DatabaseError)
def special_exception_handler(error):
    logging.error(
        "Database error, rolling back last transaction, error message was: " + str(error))
    db.session.rollback()
    return 'Database connection failed', 500


@app.errorhandler(PendingRollbackError)
def pending_rollback_exception_handler(error):
    logging.error(
        "Pending Rollback Exception, rolling back last transaction, error message was: " + str(error))
    db.session.rollback()
    return 'Database connection failed', 500


@app.errorhandler(DisconnectionError)
def disconnection_exception_handler(error):
    logging.error(
        "Disconnection Exception, rolling back last transaction, error message was: " + str(error))
    db.session.rollback()
    return 'Database connection failed', 500


if __name__ == '__main__':
    logging.info("Attempting to start server")
    app.run(host="0.0.0.0", port=5000)
