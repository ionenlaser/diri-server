import logging
import os
from flask import request, send_file
from sqlalchemy import func
import app


def s_download():
    """client spelling download - get request -> HÖRBEISPIELE"""

    req = request.args

    if req.get("spelling_id"):
        logging.info("Request for additional info")
        return send_additional_info(req.get("spelling_id"))
    else:
        logging.info("Request for new Spelling")

    client_id = req.get("id")
    if client_id is None:
        logging.error("Userid not included")
        return {"error": "Userid not included"}, 400
    user_obj = app.User.query.get(client_id)
    if user_obj is None:
        logging.error("User not found")
        return {"error": "User not found"}, 400

    try:
        used_spellings_ids = app.db.session.query(app.SpellingInteraction.pro_int_id).filter(
            app.SpellingInteraction.receiving_user == client_id).all()
    except Exception as e:
        logging.error(e)
        logging.error("Rollback transaction")
        app.db.session.rollback()

    used_spellings = []
    for spelling in used_spellings_ids:
        used_spellings.append(spelling[0])
    logging.info(
        "Player has downloaded this many spellings already: " + str(len(used_spellings)))
    chosen_pro_int = app.db.session.query(app.PronunciationInteraction).order_by(func.random()).filter(
        app.PronunciationInteraction.id.notin_(used_spellings),
        app.PronunciationInteraction.recording.is_not(None),
        app.PronunciationInteraction.skipped.is_not(True)
    ).first()

    if chosen_pro_int is None:
        logging.error("User: " + user_obj.uuid + " has no unplayed spellings")
        return {"error": "No new spellings available"}, 400

    send_spelling_int = app.SpellingInteraction(receiving_user=client_id,
                                                pro_int_id=chosen_pro_int.id)
    app.db.session.add(send_spelling_int)
    app.db.session.commit()

    logging.info("Chosen file for s_download: " +
                 str(chosen_pro_int.recording))
    path = os.path.join(app.UPLOAD_DIR, str(chosen_pro_int.recording))
    return send_file(path, as_attachment=True, attachment_filename=str(send_spelling_int.id)), 200


def send_additional_info(spelling_id):
    for i in range(3):
        
        spelling = app.SpellingInteraction.query.get(spelling_id)
        if spelling is not None:
            pro_int = app.PronunciationInteraction.query.get(spelling.pro_int_id)
            if pro_int is None:
                logging.error("Corresponding Pronunciation_Interaction doesn't exists")
                return {"error": "Corresponding Pronunciation_Interaction doesn't exists"}, 400
            pronunciation = app.Pronunciation.query.get(pro_int.pro_id)
            obj = {
                "lexeme": pronunciation.lexeme,
                "lang": pronunciation.language,
                "ipa": pronunciation.ipa
            }
            return obj, 200
            
    if spelling is None:
        logging.error("Spelling id doesn't exists")
        return {"error": "Spelling id doesn't exists"}, 400
    


def s_upload():
    """client spelling upload - post request"""

    data = request.get_json()
    logging.info("s_upload data received: " + str(data))
    client_id = data.get("client_id")
    if client_id is None:
        logging.error("Userid not included")
        return {"error": "Userid not included"}, 400
    user_obj = app.User.query.get(client_id)
    if user_obj is None:
        logging.error("User not found")
        return {"error": "User not found"}, 400

    spellings_data = data.get("spellings")
    if spellings_data is None:
        logging.error("There is no spelling object in this request")
        return {"error": "There is no spelling object in this request"}

    success_counter = 0
    for spelling in spellings_data:
        logging.info("New spelling uploaded: " + str(spelling))
        s_id = spelling.get("id")
        skipped = spelling.get("skipped")
        inputs = spelling.get("inputs")

        spelling = app.SpellingInteraction.query.get(s_id)
        if spelling is None:
            logging.error("Spelling doesn't exist, ID: " + s_id)
        else:
            success_counter += 1
            spelling.skipped = skipped
            spelling.inputs = inputs
            app.db.session.add(spelling)
    app.db.session.commit()
    response_obj = {
        "Uploaded": len(spellings_data),
        "Successful": success_counter
    }
    return response_obj, 200
