import logging
import uuid
from flask import request
import app


def user_management():
    req = request.args
    user_lang = req.get("userlang")
    game_lang = req.get("gamelang")
    if req.keys().__contains__("id"):
        # Update existing user preferences
        logging.info("Updating user preferences")
        return update_user(req.get("id"), user_lang, game_lang)
    else:
        # Create new user
        logging.info("Create new user")
        return create_user(user_lang, game_lang)


def update_user(client_id, user_lang, game_lang):
    selected_user = app.User.query.get(client_id)
    selected_user.game_lang = game_lang
    selected_user.user_lang = user_lang
    app.db.session.add(selected_user)
    app.db.session.commit()
    userdata = {
        "id": client_id,
        "gamelang": game_lang,
        "userlang": user_lang,
        "points": selected_user.points
    }
    return userdata, 200


def create_user(user_lang, game_lang):
    new_id = uuid.uuid4()
    new_user = app.User(uuid=new_id, user_lang=user_lang, game_lang=game_lang)
    app.db.session.add(new_user)
    app.db.session.commit()
    userdata = {
        "id": new_id,
        "gamelang": game_lang,
        "userlang": user_lang,
        "points": 0
    }
    return userdata, 200
