import logging

from SPARQLWrapper import SPARQLWrapper, JSON


def download_ipa_examples(lang):
    """
SELECT DISTINCT ?item ?itemLabel ?IPA_Transkription WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
  ?item wdt:P898 ?IPA_Transkription.
}
LIMIT 5000
    """
    query_string_de = """
    SELECT DISTINCT ?l ?lemma ?ipa ?audio WHERE {
    ?l dct:language wd:Q188;
    wikibase:lemma ?lemma; 
    ontolex:lexicalForm ?form .
    ?form ontolex:representation ?lemma .
    ?form wdt:P898 ?ipa.
    MINUS {?l wikibase:lexicalCategory wd:Q62155.}
    OPTIONAL{ ?form wdt:P443 ?audio .} .
    FILTER regex(?lemma, "^[A-z].*")
    }
    LIMIT 1000
    """
    query_string_en = """
    SELECT DISTINCT ?l ?lemma ?ipa ?audio WHERE {
    ?l dct:language wd:Q1860;
    wikibase:lemma ?lemma; 
    ontolex:lexicalForm ?form .
    ?form ontolex:representation ?lemma .
    ?form wdt:P898 ?ipa.
    MINUS {?l wikibase:lexicalCategory wd:Q62155.}
    OPTIONAL{ ?form wdt:P443 ?audio .} .
    FILTER regex(?lemma, "^[A-z].*")
    }
    LIMIT 1000
    """

    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")

    if lang == "en":
        sparql.setQuery(query_string_en)
    elif lang == "de":
        sparql.setQuery(query_string_de)
    else:
        logging.error("No possible language selected, returning!")

    sparql.setReturnFormat(JSON)

    logging.info("Downloading from Wikidata Lexemes in language: " + lang)
    try:
        ret = sparql.query().convert()
        logging.info("Received some results length: " +
                     str(len(ret["results"]["bindings"])))
        return ret["results"]["bindings"]
    except Exception as e:
        logging.error(e)
        exit(1)
