import json
import logging
import os
import uuid
from flask import request
from werkzeug.utils import secure_filename
import app
from app import Pronunciation, PronunciationInteraction, db


def pronunciations_convert_to_json(query_results):
    formatted_results_list = []
    for result in query_results:
        formatted_result = {"id": result.id,
                            "lexeme": result.lexeme,
                            "ipa": result.ipa,
                            "lang": result.language}
        formatted_results_list.append(formatted_result)

    result_as_json = {
        "additional_info": "hello",
        "pronunciations": formatted_results_list
    }
    return result_as_json


def p_download():
    """Answering pronunciation get request"""
    client_id = request.args.get("id")
    if client_id is None:
        logging.error("Userid not included")
        return {"error": "Userid not included"}, 400
    user_obj = app.User.query.get(client_id)
    if user_obj is None:
        logging.error("User not found")
        return {"error": "User not found"}, 400

    logging.info(user_obj)
    used_pronunciations = db.session.query(Pronunciation.id).filter(
        PronunciationInteraction.pro_id == Pronunciation.id,
        PronunciationInteraction.user_id == client_id).subquery()

    possible_pronunciations = db.session.query(Pronunciation) \
        .filter(Pronunciation.id.notin_(used_pronunciations), Pronunciation.language == user_obj.game_lang).limit(40)

    text = pronunciations_convert_to_json(possible_pronunciations)

    for pro in possible_pronunciations:
        pro_to_u = app.PronunciationInteraction(
            pro_id=pro.id, user_id=client_id)
        db.session.add(pro_to_u)
    db.session.commit()

    return text, 200


def p_upload():
    """answering pronunciation post request"""
    # process text data
    text_data = request.form.get("info")
    json_data = json.loads(text_data)
    client_id = json_data.get("client_id")
    if client_id is None:
        logging.error("Userid not included")
        return {"error": "Userid not included"}, 400
    user_obj = app.User.query.get(client_id)
    if user_obj is None:
        logging.error("User not found")
        return {"error": "User not found"}, 400

    pronunciations_data = json_data.get("pronunciations")
    for pro in pronunciations_data:
        logging.info(pro)
        pro_id = pro.get("pro_id")
        skipped = pro.get("skipped")

        # Access related obj
        pro_int = PronunciationInteraction.query.filter(
            PronunciationInteraction.pro_id == pro_id,
            PronunciationInteraction.user_id == client_id).first()

        if pro_int is None:
            return {"error": "Upload could not reference original ipa pronunciations"}, 400
        else:

            pro_int.skipped = skipped

            if not skipped:
                # use secure filename to negate any unsafe filenames
                f_name = secure_filename(pro.get("f_name"))
                if f_name != "" and f_name in request.files.keys():
                    f = request.files[f_name]
                    server_filename = str(uuid.uuid4()) + ".mp4"
                    f.save(os.path.join(app.UPLOAD_DIR, server_filename))
                    pro_int.recording = server_filename
            db.session.add(pro_int)
    db.session.commit()

    return {"files_received": len(request.files.keys())}, 200
