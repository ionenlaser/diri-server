diri-server

Login Toolforge
ssh ionenlaser@login.toolforge.org

Open diri project
become diri

starting webservice
webservice --backend=kubernetes python3.9 start

hosting url
https://diri.toolforge.org/


sudo docker run -p 127.0.0.1:3306:3306 --name mariadb -e MARIADB_ROOT_PASSWORD=YOURPASSWORDHERE -e MARIADB_DATABASE=diridb -d mariadb:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

DROP TABLE IF EXISTS spelling_interaction, pronunciation_interaction, pronunciation, user;